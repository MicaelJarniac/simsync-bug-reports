# SimSync bug tracker

## How to create an issue or bug report (Discord or Application)

- ### Via the GitLab interface

You can find the issues tab on the left, or click here to go directly to it: https://gitlab.com/simsync/simsync-bug-reports/issues. You will have to sign in or create an account, which takes just a few seconds.

Then just click the **New Issue** button.

If the issue you're reporting has sensitive information (like your email or IP address), or can be exploited in any way, there is an option for confidentiality right below your message. It should look like this:

- [x] **This issue is confidential and should only be visible to team members with at least Reporter access.**

Checking it will do just what it says: no one else but our devs will be able to see it.

- ### Via email

Just send an email to `incoming+simsync-simsync-bug-reports-16391709-issue-@incoming.gitlab.com` with your report.

These are automatically confidential.


## What does a proper issue or bug report look like?

We cannot give you the exact formula for what is wrong. However, we just ask that you give as much information as possible.

##### Want us to help you?
If you're requesting us to help you with something, include your **Discord Name and Number** (example: `Sheepii#0001`), or maybe your email address. Just remember that if you're posting personal information, you should mark the issue as confidential so only our devs can see it.

## Need further help?
If you have any questions, don't hestitate to ask on our [Discord](https://discord.gg/g22dU5a), in the `#general-questions` channel, and be sure to also read our `#FAQ` channel.
